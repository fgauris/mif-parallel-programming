package main

import (
	"bufio"
	"encoding/hex"
	"fmt"
	"math"
	"sync"

	// "fmt"
	"os"
	"runtime"
	"strings"
	"time"
	//"log"
	//"sync"
)

func main() {
	testSize := 100
	duplicationLevel := 250
	runTests(testSize, duplicationLevel)
}

func runTests(testSize int, duplicationLevel int) {
	fmt.Println("Google Go tests(", duplicationLevel, "duplication): ")
	fmt.Println("1KB:")
	runTest(testSize, "..\\Data\\1KB.txt", false, duplicationLevel)
	fmt.Println("200KB:")
	runTest(testSize, "..\\Data\\200KB.txt", false, duplicationLevel)
	fmt.Println("5MB:")
	runTest(testSize, "..\\Data\\5MB.txt", false, duplicationLevel)
}

func runTest(testSize int, filePath string, printResults bool, duplicationLevel int) {
	bytes, length := readData(filePath, duplicationLevel)
	if printResults == true {
		fmt.Println("Original message:")
		printBytes(bytes, length)
	}
	var totalTime int64 = 0
	for i := 0; i < testSize; i++ {
		begin := time.Now()
		result, resultLength := encodeData(bytes, length)
		durationNano := time.Since(begin).Nanoseconds()
		totalTime += (int64)(durationNano / 10000)
		if i == 0 && printResults {
			fmt.Println("LRC message:")
			printBytes(result, resultLength)
		}
	}
	fmt.Println("Average time after", testSize, "runs:", float64(totalTime)/float64(testSize)/100, ".")
}

func printBytes(bytes []byte, length int) {
	for i := 0; i < length; i++ {
		fmt.Printf("%02X ", bytes[i])
	}
	fmt.Println()
}

func encodeData(bytes []byte, length int) ([]byte, int) {
	var resultLength int = int(math.Ceil(float64(length)/4) * 5)
	result := make([]byte, resultLength)
	concurrencyLevel := getConcurrencyLevel()
	step := length / concurrencyLevel
	var wg sync.WaitGroup
	wg.Add(concurrencyLevel)
	for i := 0; i < concurrencyLevel; i++ {
		from := i * step
		to := (i+1)*step - 1
		if i == (concurrencyLevel - 1) {
			to = length - 1
		}
		go func(from int, to int) {
			defer wg.Done()
			encodeBlock(bytes, from, to, result)
		}(from, to)
	}
	wg.Wait()

	return result, resultLength
}

func encodeBlock(bytes []byte, from int, to int, result []byte) {
	//fmt.Println("go: from ", from, " to ", to)
	var lrc byte = 0
	for i := from; i <= to; i++ {
		result[i+i/4] = bytes[i]
		lrc = lrc ^ bytes[i]
		if i%4 == 3 {
			result[i+i/4+1] = lrc
			lrc = 0
		}
	}
}

func readData(filePath string, duplicationLevel int) ([]byte, int) {
	file, err := os.Open(filePath)
	check(err)

	scanner := bufio.NewScanner(file)
	//supports ~5MB file with spaces
	buf := make([]byte, 0, 8*1024*1024)
	scanner.Buffer(buf, 8*1024*1024)
	scanner.Scan()
	check(scanner.Err())
	line := strings.ReplaceAll(scanner.Text(), " ", "")
	defer file.Close()
	originalBytes, err := hex.DecodeString(line)
	check(err)

	length := len(originalBytes)
	newArrayLength := length * duplicationLevel
	arr := make([]byte, newArrayLength)

	for i := 0; i < duplicationLevel; i++ {
		for j := 0; j < length; j++ {
			arr[i * length + j] = originalBytes[j]
		}
	}

	return arr, newArrayLength
}

func gcd(a int, b int) int {
	if b == 0 {
		return a
	}
	return gcd(b, a%b)
}

func getConcurrencyLevel() int {

	totalThreads := runtime.NumCPU()
	totalThreads = gcd(4, totalThreads)
	if totalThreads < 4 {
		totalThreads = 4
	}
	//fmt.Println("Concurrency level: ", totalThreads)
	return totalThreads
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
