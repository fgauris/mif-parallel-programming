#pragma once
#include <iostream>
#include "DataReadResult.h"
using namespace std;

class CilkTests
{
public:
	void runTests(int testSize, int duplicationLevel);
private:
	void runTest(int testSize, string filePath, bool printResults, int duplicationLevel);
	DataReadResult encodeData(DataReadResult& data);
	int gcd(int a, int b);
	void encodeBlock(unsigned char* bytes, int from, int to, unsigned char* result);
	int getConcurrencyLevel();
};

