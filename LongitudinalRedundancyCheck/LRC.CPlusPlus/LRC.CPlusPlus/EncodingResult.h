#pragma once
class EncodingResult
{
public:
	EncodingResult(unsigned char* bytes, int length) {
		this->bytes = bytes;
		this->length = length;
	}
	int bytesLength;
	unsigned char* bytes;
};

