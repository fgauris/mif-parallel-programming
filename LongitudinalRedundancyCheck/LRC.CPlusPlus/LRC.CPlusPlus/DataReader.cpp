#include "DataReader.h"
#include <iostream>
#include <fstream>
#include <string>
#include <boost/format.hpp>
#include <vector>
#include <sstream>
#include "DataReadResult.h"
using namespace std;
using namespace boost;

DataReadResult DataReader::readData(std::string filePath, int duplicationLevel) {
	std::ifstream in(filePath);
	std::string contents(static_cast<std::stringstream const&>(std::stringstream() << in.rdbuf()).str());
	std::istringstream hex_chars_stream(contents);
	std::vector<unsigned char> bytes;

	unsigned int c;
	while (hex_chars_stream >> std::hex >> c)
	{
		bytes.push_back(c);
	}
	int bytesLength = bytes.size();
	int newArrayLength = bytesLength * duplicationLevel;
	unsigned char* arr = new unsigned char[newArrayLength];
	for (size_t i = 0; i < duplicationLevel; i++)
	{
		std::copy(bytes.begin(), bytes.end(), arr + (bytesLength * i));
	}

	return DataReadResult(arr, newArrayLength);
}

void DataReader::printBytes(DataReadResult& data) {
	for (int i = 0; i < data.length; ++i)
	{
		//std::cout << std::hex << (int)data.bytes[i] << " ";
		std::cout << boost::format("%02X ") % (int)data.bytes[i];
	}
	cout << endl;
}

void DataReader::saveToFile(DataReadResult& data, std::string filePath) {
	//std::string contents("");
	std::ofstream out(filePath);
	for (int i = 0; i < data.length; ++i)
	{
		if (i + 1 == data.length)
		{
			out << boost::format("%02X") % (int)data.bytes[i];
		}
		else
		{
			out << boost::format("%02X ") % (int)data.bytes[i];
		}
	}
}