// LRC.CPlusPlus.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "SerialTests.h"
#include "CilkTests.h"
#include "TbbTests.h"
#include "OpenMpTests.h"

int main()
{
	int testSize = 100;//100
	int duplicationLevel = 250;//250

	SerialTests serialTests = SerialTests();
	serialTests.runTests(testSize, duplicationLevel);

	CilkTests cilkTests = CilkTests();
	cilkTests.runTests(testSize, duplicationLevel);

	TbbTests tbbTests = TbbTests();
	tbbTests.runTests(testSize, duplicationLevel);

	OpenMpTests ompTests = OpenMpTests();
	ompTests.runTests(testSize, duplicationLevel);

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
