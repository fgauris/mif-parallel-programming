#include "SerialTests.h"
#include "DataReadResult.h"
#include "DataReader.h"
#include <iostream>
#include <boost/format.hpp>
#include <cmath>
#include <chrono>
#include <thread>

void SerialTests::runTests(int testSize, int duplicationLevel)
{
	cout << boost::format("Serial tests(%1% duplication): ") % duplicationLevel << endl;
	cout << "1KB:" << endl;
	runTest(testSize, ".\\Data\\1KB.txt", false, duplicationLevel);
	cout << "200KB:" << endl;
	runTest(testSize, ".\\Data\\200KB.txt", false, duplicationLevel);
	cout << "5MB:" << endl;
	runTest(testSize, ".\\Data\\5MB.txt", false, duplicationLevel);
	/*cout << "15MB:" << endl;
	runTest(testSize, ".\\Data\\15MB.txt", false, duplicationLevel);
	cout << "30MB:" << endl;
	runTest(testSize, ".\\Data\\30MB.txt", false, duplicationLevel);
	cout << "100MB:" << endl;
	runTest(testSize, ".\\Data\\100MB.txt", false, duplicationLevel);*/
	cout << endl;
}


void SerialTests::runTest(int testSize, string filePath, bool printResults, int duplicationLevel) {
	int c = 0;
	DataReader reader = DataReader();
	DataReadResult data = reader.readData(filePath, duplicationLevel);
	unsigned char* bytes = data.bytes;
	//cout << "Original message:" << endl;
	//reader.printBytes(data);
	
	int totalTime = 0;
	for (int i = 0; i < testSize; i++)
	{
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
		DataReadResult result = encodeData(data);
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		totalTime += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
		if (i == 0 && printResults)
		{
			cout << "LRC message:" << endl;
			reader.printBytes(result);
			/*cout << "Saving to file..." << endl;
			reader.saveToFile(result, filePath + ".Result.txt");
			cout << "Saving completed..." << endl;*/
		}
	}

	std::cout << "Average time after " << testSize << " runs: " << (totalTime * 1.0 / testSize) << " ms." << std::endl;
}

DataReadResult SerialTests::encodeData(DataReadResult& data) {
	unsigned char* bytes = data.bytes;
	int length = data.length;
	int resultLength = ceil(length * 1.0 / 4) * 5;
	unsigned char* result = new unsigned char[resultLength];
	int concurrencyLevel = getConcurrencyLevel();
	int step = length / concurrencyLevel;
	for (int i = 0; i < concurrencyLevel; i++)
	{
		int from = i * step;
		int to = (i + 1) * step - 1;
		if (i == concurrencyLevel - 1)
			to = length - 1;
		encodeBlock(data.bytes, from, to, result);
	}

	return DataReadResult(result, resultLength);
}

void SerialTests::encodeBlock(unsigned char* bytes, int from, int to, unsigned char* result) {
	//cout << boost::format("serial: from: %1%, to: %2%\n") % from % to;
	int lrc = 0;
	for (int i = from; i <= to; i++)
	{
		result[i + i / 4] = bytes[i];
		lrc ^= bytes[i];
		if (i % 4 == 3) {
			result[i + i / 4 + 1] = lrc;
			lrc = 0;
		}
	}
}

int SerialTests::gcd(int a, int b) {
	if (b == 0)
		return a;
	return gcd(b, a % b);
}

int SerialTests::getConcurrencyLevel() {
	unsigned totalThreads = std::thread::hardware_concurrency();
	totalThreads = gcd(4, totalThreads);
	if (totalThreads < 4)
		totalThreads = 4;

	return totalThreads;
}








// Old serial version
//DataReadResult SerialTests::encodeData(DataReadResult& data) {
//	unsigned char lrc = 0;
//	unsigned char* bytes = data.bytes;
//	int length = data.length;
//	int resultLength = ceil(length*1.0/4)*5;
//	unsigned char* result = new unsigned char[resultLength];
//	unsigned char byte;
//	for (int i = 0; i < length; i++)
//	{
//		byte = bytes[i];
//		result[i + i / 4] = byte;
//		lrc ^= byte;
//		if (i % 4 == 3) {
//			result[i + i / 4 + 1] = lrc;
//			lrc = 0;
//		}
//	}
//
//	return DataReadResult(result, resultLength);
//}

