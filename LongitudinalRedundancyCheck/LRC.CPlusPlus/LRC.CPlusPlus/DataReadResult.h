#pragma once
class DataReadResult
{
public:
	DataReadResult(unsigned char* bytes, int length) {
		this->bytes = bytes;
		this->length = length;
	}
	~DataReadResult() {
		delete this->bytes;

	}
	int length;
	unsigned char* bytes;
};

