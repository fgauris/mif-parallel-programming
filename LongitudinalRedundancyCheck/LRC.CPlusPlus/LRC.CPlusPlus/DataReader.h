#pragma once
#include <string>
#include "DataReadResult.h"
using namespace std;


class DataReader
{
public:
	DataReadResult readData(std::string filePath, int duplicationLevel);
	void printBytes(DataReadResult& data);
	void saveToFile(DataReadResult& data, std::string filePath);
};

