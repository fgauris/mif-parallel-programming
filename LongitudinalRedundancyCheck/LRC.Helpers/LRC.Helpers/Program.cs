﻿using System;
using System.IO;
using System.Linq;

namespace LRC.HexNumbersGenerator
{
    class Program
    {
        static Random random = new Random();
        static void Main(string[] args)
        {
            var location = "C:\\Source\\Magistrinis\\LongitudinalRedundancyCheck\\Data\\";
            Console.WriteLine($"Generating files at '{location}'...");
            //Console.WriteLine("200KB...");
            //File.WriteAllText(location + "200KB.txt", GetRandomHexNumber(200*1000));
            //Console.WriteLine("5MB...");
            //File.WriteAllText(location + "5MB.txt", GetRandomHexNumber(5*1000*1000));
            //Console.WriteLine("15MB...");
            //File.WriteAllText(location + "15MB.txt", GetRandomHexNumber(15*1000*1000));
            //Console.WriteLine("30MB...");
            //File.WriteAllText(location + "30MB.txt", GetRandomHexNumber(30*1000*1000));
            Console.WriteLine("100MB...");
            File.WriteAllText(location + "100MB.txt", GetRandomHexNumber(100 * 1000 * 1000));
            Console.WriteLine("Generation finished! Press anything to complete...");
            Console.Read();
        }

        public static string GetRandomHexNumber(int digits)
        {
            var buffer = new byte[digits / 2];
            random.NextBytes(buffer);
            var result = string.Join(" ", buffer.Select(x => x.ToString("X2")).ToArray());
            if (digits % 2 == 0)
                return result;
            return result + random.Next(16).ToString("X");
        }
    }
}
