package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"sync"
	"time"

	// "fmt"
	"os"
	"strings"
	//"log"
	//"sync"
)

var cutOff int = 100

func main() {
	testSize := 100
	runTests(testSize)
}

func runTests(testSize int) {
	//fmt.Println("Google Go tests(", duplicationLevel, "duplication): ")
	fmt.Println("Google Go tests:")
	fmt.Println("10:")
	runTest(testSize, "..\\Data\\10.txt", false)
	//fmt.Println("20:")
	//runTest(testSize, "..\\Data\\20.txt", true)
	fmt.Println("2K:")
	runTest(testSize, "..\\Data\\2K.txt", false)
	fmt.Println("200K:")
	runTest(testSize, "..\\Data\\200K.txt", false)
	fmt.Println("500K:")
	runTest(testSize, "..\\Data\\500K.txt", false)
	fmt.Println("5M:")
	runTest(testSize, "..\\Data\\5M.txt", false)
	fmt.Println("20M:")
	runTest(testSize, "..\\Data\\20M.txt", false)

}

func runTest(testSize int, filePath string, printResults bool) {
	numbers, _ := readData(filePath)
	if printResults == true {
		fmt.Println("Original numbers:")
		printNumbers(numbers)
	}

	var totalTime int64 = 0
	for i := 0; i < testSize; i++ {
		copy := numbers
		begin := time.Now()
		quicksortParallel(copy)
		durationNano := time.Since(begin).Nanoseconds()
		totalTime += (int64)(durationNano / 10000)
		if i == 0 && printResults {
			fmt.Println("Sorted numbers:")
			printNumbers(copy)
		}
	}
	fmt.Println("Average time after", testSize, "runs:", float64(totalTime)/float64(testSize)/100, ".")
}

func quicksortParallel(arr []int){
	var wg sync.WaitGroup
	quicksort(arr, 0, len(arr)-1, wg)
	wg.Wait()
}

func quicksort(arr []int, low int, high int, group sync.WaitGroup){
	if low < high {
		/* pi is partitioning index, arr[p] is now
		   at right place */
		pi := partition(arr, low, high, group)

		// Separately sort elements before
		// partition and after partition
		if (high - low) < cutOff{
			quicksort(arr, low, pi - 1, group)
		} else {
			group.Add(1)
			go func (arr []int, low int, pi int, group sync.WaitGroup ){
				defer group.Done()
				quicksort(arr, low, pi - 1, group)
			}(arr, low, pi, group)
		}

		quicksort(arr, pi+1, high, group)
	}
}



func readData(filePath string) ([]int, int) {
	file, err := os.Open(filePath)
	check(err)
	defer file.Close()
	reader := bufio.NewReader(file)
	var arrayLengthS string
	arrayLengthS, err = reader.ReadString('\n')
	arrayLengthS = strings.ReplaceAll(arrayLengthS, "\r\n", "")
	check(err)
	var arrayLength int
	arrayLength, err = strconv.Atoi(arrayLengthS)
	check(err)
	arr := make([]int, arrayLength)
	var line string
	i := 0
	for {

		line, err = reader.ReadString('\n')
		line = strings.ReplaceAll(line, "\r\n", "")

		strs := strings.Split(line, " ")
		var err1 error
		for j := 0; j < len(strs);j++  {
			if strs[j] != "" {
				arr[i], err1 = strconv.Atoi(strs[j])
				check(err1)
				i++
			}
		}

		if err != nil {
			break
		}
	}

	//fmt.Printf("\tRead %d numbers\n", len(arr))

	//fmt.Printf(" \tLast element: %v \n", arr[len(arr)-1])

	if err != io.EOF {
		fmt.Printf("Failed!: %v\n", err)
	}

	return arr, arrayLength
}

func printNumbers(numbers []int) {
	length := len(numbers)
	for i := 0; i < length; i++ {
		fmt.Printf("%v ", numbers[i])
	}
	fmt.Println()
}

func partition(arr []int, low int, high int, group sync.WaitGroup) int {
	pivot := arr[high] // pivot
	i := low - 1 // index of smaller element

	for j := low; j <= high-1; j++ {
		// If current element is smaller than or equal to pivot
		if arr[j] <= pivot {
			i++
			swap(arr, i, j)
		}
	}
	swap(arr, i+1, high)
	return i+1
}

func swap(arr []int, a int, b int){
	t := arr[a]
	arr[a] = arr[b]
	arr[b] = t
}

//func encodeData(bytes []int, length int) ([]int, int) {
//	var resultLength int = int(math.Ceil(float64(length)/4) * 5)
//	result := make([]byte, resultLength)
//	concurrencyLevel := getConcurrencyLevel()
//	step := length / concurrencyLevel
//	var wg sync.WaitGroup
//	wg.Add(concurrencyLevel)
//	for i := 0; i < concurrencyLevel; i++ {
//		from := i * step
//		to := (i+1)*step - 1
//		if i == (concurrencyLevel - 1) {
//			to = length - 1
//		}
//		go func(from int, to int) {
//			defer wg.Done()
//			encodeBlock(bytes, from, to, result)
//		}(from, to)
//	}
//	wg.Wait()
//
//	return result, resultLength
//}
//
//func encodeBlock(bytes []byte, from int, to int, result []byte) {
//	//fmt.Println("go: from ", from, " to ", to)
//	var lrc byte = 0
//	for i := from; i <= to; i++ {
//		result[i+i/4] = bytes[i]
//		lrc = lrc ^ bytes[i]
//		if i%4 == 3 {
//			result[i+i/4+1] = lrc
//			lrc = 0
//		}
//	}
//}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func Map(vs []string, f func(string) int) []int {
	vsm := make([]int, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}

