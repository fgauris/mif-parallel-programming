#include "SerialTests.h"
#include "DataReadResult.h"
#include "DataReader.h"
#include "SortHelper.h"
#include <iostream>
#include <boost/format.hpp>
#include <cmath>
#include <chrono>
#include <thread>

void SerialTests::runTests(int testSize, TestData* testData) 
{
	cout << boost::format("Serial tests:") << endl;
	cout << "10:" << endl;
	runTest(testSize, testData->Size10, false);
	cout << "2K:" << endl;
	runTest(testSize, testData->Size2K, false);
	cout << "200K:" << endl;
	runTest(testSize, testData->Size200K, false);
	cout << "500K:" << endl;
	runTest(testSize, testData->Size500K, false);
	cout << "5M:" << endl;
	runTest(testSize, testData->Size5M, false);
	cout << "20M:" << endl;
	runTest(testSize, testData->Size20M, false);
	/*cout << "100M:" << endl;
	runTest(testSize, "..\\..\\Data\\100M.txt", false);*/
	cout << endl;
}

void SerialTests::runTest(int testSize, DataReadResult* data, bool printData) {
	DataReader reader = DataReader();
	int* numbers = data->numbers;
	if (printData)
	{
		cout << "Original numbers:" << endl;
		reader.printNumbers(*data);
	}

	int totalTime = 0;
	for (int i = 0; i < testSize; i++)
	{
		DataReadResult copy = reader.copyArray(*data);
		if (i == 1 && printData)
		{
			cout << "Copied numbers:" << endl;
			reader.printNumbers(copy);
		}
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
		quickSortSerial(copy);
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		totalTime += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
		if (i == 1 && printData)
		{
			cout << "Sorted numbers:" << endl;
			reader.printNumbers(copy);
			/*cout << "Saving to file..." << endl;
			reader.saveToFile(result, filePath + ".Result.txt");
			cout << "Saving completed..." << endl;*/
		}
	}

	std::cout << "Average time after " << testSize << " runs: " << (totalTime * 1.0 / testSize) << " ms." << std::endl;
}

void SerialTests::quickSortSerial(DataReadResult& data) {
	quickSort(data, 0, data.length - 1);
}

void SerialTests::quickSort(DataReadResult& data, int low, int high) {
	if (low < high)
	{
		/* pi is partitioning index, arr[p] is now
		   at right place */
		int pi = SortHelper::partition(data.numbers, low, high);

		// Separately sort elements before 
		// partition and after partition 
		quickSort(data, low, pi - 1);
		quickSort(data, pi + 1, high);
	}
}



