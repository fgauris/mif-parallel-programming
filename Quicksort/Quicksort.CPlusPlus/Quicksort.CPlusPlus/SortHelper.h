#pragma once
class SortHelper
{
public:
	static int partition(int* arr, int low, int high);
	static void swap(int* a, int* b);
	static const int cutOff = 100;
};

