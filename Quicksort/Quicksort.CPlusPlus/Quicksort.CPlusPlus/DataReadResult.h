#pragma once
class DataReadResult
{
public:
	DataReadResult(int* numbers, int length) {
		this->numbers = numbers;
		this->length = length;
	}
	~DataReadResult() {
		delete this->numbers;
	}
	int length;
	int* numbers;
};
