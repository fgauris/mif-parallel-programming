#include "DataReader.h"
#include <iostream>
#include <fstream>
#include <string>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <sstream>
#include "DataReadResult.h"
#include <stdexcept>
using namespace std;
using namespace boost;


TestData* DataReader::readAllData() 
{
	cout << "Reading 10..." << endl;
	DataReadResult* size10 = readData("..\\..\\Data\\10.txt");
	cout << "Reading 2K..." << endl;
	DataReadResult* size2K = readData("..\\..\\Data\\2K.txt");
	cout << "Reading 200K..." << endl;
	DataReadResult* size200K = readData("..\\..\\Data\\200K.txt");
	cout << "Reading 500K..." << endl;
	DataReadResult* size500K = readData("..\\..\\Data\\500K.txt");
	cout << "Reading 5M..." << endl;
	DataReadResult* size5M = readData("..\\..\\Data\\5M.txt");
	cout << "Reading 20M..." << endl;
	DataReadResult* size20M = readData("..\\..\\Data\\20M.txt");

	TestData* result = new TestData(size10, size2K, size200K, size500K, size5M, size20M);

	return result;
}

DataReadResult* DataReader::readData(std::string filePath) {
	ifstream myReadFile;
	myReadFile.open(filePath);
	string line;
	int* arr;
	int arrLength = 0;
	int lineLength = 0;
	int start = 0;
	int end = 0;
	if (myReadFile.is_open()) {
		getline(myReadFile, line);
		arrLength = stoi(line);
		arr = new int[arrLength];

		while (getline(myReadFile, line)) {
			vector<string> strs;
			vector<string> lineItems = split(strs, line, is_any_of(" "));
			if (lineItems[lineItems.size() - 1].empty())
				lineItems.pop_back();
			lineLength = lineItems.size();
			end += lineLength;
			for (int i = 0; i < lineLength; i++)
			{
				arr[start+i] = stoi(lineItems[i]);
			}
			start = end;
		}
	}
	else
	{
		throw std::invalid_argument("File not found! File: " + filePath);
	}
	return new DataReadResult(arr, arrLength);
}

void DataReader::printNumbers(DataReadResult& data) {
	for (int i = 0; i < data.length; ++i)
	{
		std::cout << (int)data.numbers[i] << " ";
	}
	cout << endl;
}

DataReadResult DataReader::copyArray(DataReadResult& data) {
	int* dest = new int[data.length];
	std::copy(data.numbers, data.numbers + data.length, dest);
	return DataReadResult(dest, data.length);
}
