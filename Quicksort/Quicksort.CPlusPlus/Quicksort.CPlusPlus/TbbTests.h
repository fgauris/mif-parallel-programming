#pragma once
#include <iostream>
#include "DataReadResult.h"
#include "TestData.h"
#include "tbb/task_group.h"
using namespace tbb;
using namespace std;

class TbbTests
{
public:
	void runTests(int testSize, TestData* testData);
private:
	void runTest(int testSize, DataReadResult* data, bool printData);
	void quickSortParallel(DataReadResult& data);
	void quickSort(DataReadResult& data, int low, int high, task_group& g);
};


