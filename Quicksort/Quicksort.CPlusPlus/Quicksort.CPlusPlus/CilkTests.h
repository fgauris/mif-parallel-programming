#pragma once
#include <iostream>
#include "DataReadResult.h"
#include "TestData.h"
using namespace std;

class CilkTests
{
public:
	void runTests(int testSize, TestData* testData);
private:
	void runTest(int testSize, DataReadResult* data, bool printData);
	void quickSortParallel(DataReadResult& data);
	void quickSort(DataReadResult& data, int low, int high);
};

