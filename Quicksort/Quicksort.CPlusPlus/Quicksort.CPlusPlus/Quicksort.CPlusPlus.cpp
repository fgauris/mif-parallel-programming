// Quicksort.CPlusPlus.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//https://www.geeksforgeeks.org/cpp-program-for-quicksort/
//https://github.com/eduardlopez/quicksort-parallel/blob/master/quicksort-omp.h
//https://stackoverflow.com/questions/8023135/c-openmp-parallel-quicksort
#include <iostream>
#include "SerialTests.h"
#include "CilkTests.h"
#include "TbbTests.h"
#include "OpenMpTests.h"
#include "DataReader.h"
#include "TestData.h"

int main()
{
	try
	{
		int testSize = 100;//100
		DataReader reader = DataReader();
		TestData* data = reader.readAllData();


		SerialTests serialTests = SerialTests();
		serialTests.runTests(testSize, data);

		TbbTests tbbTests = TbbTests();
		tbbTests.runTests(testSize, data);

		OpenMpTests ompTests = OpenMpTests();
		ompTests.runTests(testSize, data);

		CilkTests cilkTests = CilkTests();
		cilkTests.runTests(testSize, data);
		delete data;
	}
	catch (const std::exception& s)
	{
		cout << s.what() << endl;
	}
}




// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
