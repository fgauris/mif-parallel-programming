#pragma once
#include <string>
#include "DataReadResult.h"
#include "TestData.h"
class DataReader
{
public:
	TestData* readAllData();
	DataReadResult* readData(std::string filePath);
	void printNumbers(DataReadResult& data);
	DataReadResult copyArray(DataReadResult& data);
};

