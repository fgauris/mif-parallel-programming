#pragma once
#include "MatrixReader.h"
#include <iostream>
#include <fstream>
#include <string>
#include <boost/algorithm/string.hpp>
#include <vector>
#include "MatricesReadResult.h"
using namespace std;
using namespace boost;

class MatrixReader
{
private:
	int** ReadMatrix(int x, int y, ifstream& file);

public:
	MatricesReadResult ReadMatrices(std::string filePath);
	void PrintMatrix(int** matrix, int x, int y);
};
