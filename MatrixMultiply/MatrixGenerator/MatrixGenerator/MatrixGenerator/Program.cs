﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MatrixGenerator
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //if (args.Length != 4)
            //    throw new ArgumentException($"Missing arguments. Need m, n, p, filepath. Received only {args.Length} arguments.", nameof(args));
            //var m = int.Parse(args[0]);
            //var n = int.Parse(args[1]);
            //var p = int.Parse(args[2]);
            //var filePath = args[3];

            var appParams = new List<AppParameters>
            {
                //new AppParameters(1,2,1),
                //new AppParameters(5,4,3),
                //new AppParameters(10,15,11),
                //new AppParameters(40,38,45),
                //new AppParameters(90,120,60),
                //new AppParameters(110,90,160),
                //new AppParameters(400,500,600),
                //new AppParameters(900,1100,1000),
                //new AppParameters(2000,2300,1900),
                new AppParameters(3500,2900,3200),
                new AppParameters(9500,9900,9200),
                new AppParameters(19500,19900,15200)
            };


            var tasks = appParams.Select(p => Task.Run(() => DoWork(p))).ToArray();
            await Task.WhenAll(tasks).ConfigureAwait(false);
            Console.WriteLine("Finished! Press any key to end.");
            Console.Read();
        }

        public static void DoWork(AppParameters parameters)
        {
            var key = parameters.Key;
            var m = parameters.M;
            var n = parameters.N;
            var p = parameters.P;
            var filePath = parameters.FilePath;
            Console.WriteLine($"{key}: Parameters: m={m}, n={n}, p={p}, filepath='{filePath}'.");
            Console.WriteLine($"{key}: Generating matrix A...");
            var A = Generate(m, n);
            Console.WriteLine($"{key}: Generating matrix B...");
            var B = Generate(n, p);
            Console.WriteLine($"{key}: Multiplying matrices...");
            var C = Multiply(A, B, m, n, p);
            Console.WriteLine($"{key}: Saving to file...");
            SaveToFile(A, B, C, filePath);
        }



        private static void SaveToFile(int[,] A, int[,] B, int[,] C, string filePath)
        {
            var m = A.GetLength(0);
            var n = A.GetLength(1);
            var p = B.GetLength(1);
            using var file = new StreamWriter(filePath, false);
            file.WriteLine($"{m} {n} {p}");
            file.WriteLine("---");
            WriteMatrix(A, file);
            file.WriteLine();
            file.WriteLine("---");
            WriteMatrix(B, file);
            file.WriteLine();
            file.WriteLine("---");
            WriteMatrix(C, file);
        }

        private static void WriteMatrix(int[,] M, TextWriter file)
        {
            var x = M.GetLength(0);
            var y = M.GetLength(1);
            for (var i = 0; i < x; i++)
            {
                for (var j = 0; j < y; j++)
                {
                    if (j != 0)
                        file.Write(" ");
                    file.Write(M[i, j]);
                }
                if (i + 1 != x)
                    file.WriteLine();
            }
        }

        public static int[,] Generate(int m, int n)
        {
            var r = new Random();
            var M = new int[m, n];
            for (var i = 0; i < m; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    M[i, j] = r.Next(1, 20);
                }
            }

            return M;
        }

        public static int[,] Multiply(int[,] A, int[,] B, int m, int n, int p)
        {
            var C = new int[m, p];
            for (var i = 0; i < m; i++)
            {
                for (var j = 0; j < p; j++)
                {
                    C[i, j] = 0;
                    for (var k = 0; k < n; k++)
                    {
                        C[i, j] += A[i, k] * B[k, j];
                    }
                }
            }

            return C;
        }
    }

    public class AppParameters
    {
        public AppParameters(int m, int n, int p)
        {
            M = m;
            N = n;
            P = p;
            FilePath = $@"C:\Source\Magistrinis\MatrixMultiply\MatrixGenerator\MatrixGenerator\Matrices\{Key}.txt";
        }

        public string FilePath { get; }

        public string Key => $"{M}x{N}x{P}";

        public int M { get; }

        public int N { get; }

        public int P { get; }
    }
}
