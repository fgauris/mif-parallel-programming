package main

import (
	"bufio"
	"fmt"
	"sync"

	// "fmt"
    "os"
	"strconv"
	"strings"
	"time"
	//"log"
	//"sync"
)

func main() {

	// readFile("1x2x1.txt")
	// readFile("5x4x3.txt")
	// readFile("10x15x11")
	// readFile("40x38x45")

	testSize:=100
	fmt.Print("90x120x60: ")
	runTest(testSize, "C:\\Source\\Magistrinis\\MatrixMultiply\\Matrices\\90x120x60.txt")
	fmt.Print("400x500x600: ")
	runTest(testSize, "C:\\Source\\Magistrinis\\MatrixMultiply\\Matrices\\400x500x600.txt")
	fmt.Print("900x1100x1000: ")
	runTest(testSize, "C:\\Source\\Magistrinis\\MatrixMultiply\\Matrices\\900x1100x1000.txt")
}

func runTest(testSize int, fileName string){
	readFile(fileName)
	A, B, m, n, p := readFile(fileName)

	var totalTime int = 0.0
	for i := 0; i < testSize; i++ {
		begin := time.Now()

		multiplyParallelPar(A, B, m, n, p)
		durationNano :=time.Since(begin).Nanoseconds()
		totalTime += (int)(durationNano / 10000)

	}

	fmt.Println("Average time after", testSize, "runs:", float32(totalTime) / float32(testSize) / 100, ".")

}

func multiplyParallelPar(A [][]int, B [][]int, m int, n int, p int) [][]int{
	C := make([][]int, m)
	for l := 0; l < m; l++ {
		C[l] = make([]int, p)
	}
	var wg sync.WaitGroup
	wg.Add(m*p)
	for i := 0; i < m; i++ {
		go func(i int) {
			for j := 0; j < p; j++ {
				go func(i, j int){
					defer wg.Done()
					C[i][j] = 0
					for k := 0; k < n; k++ {
						C[i][j] += A[i][k] * B[k][j]
					}
				}(i,j)
			}
		}(i)
	}
	wg.Wait()
	return C
}

//func multiply() {
//	for i := 0; i < m; i++ {
//		for j := 0; j < p; j++ {
//			matC[i][j] = 0
//			for k := 0; k < n; k++ {
//				matC[i][j] = matC[i][j] + (matA[i][k] * matB[k][j])
//			}
//		}
//	}
//}

func readFile(fileName string) ([][]int, [][]int, int, int, int) {
	var A [][]int
	var B [][]int
	file, err := os.Open(fileName)
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)

	scanner.Scan()
	var block1 []string = strings.Split(scanner.Text(), " ")
	var m int
	var n int
	var p int
	m, err = strconv.Atoi(block1[0])
	n, err = strconv.Atoi(block1[1])
	p, err = strconv.Atoi(block1[2])

	scanner.Scan() //---

	//matA n x m
	var block2 []string 
	
	for i := 0; i < m; i++ {
		scanner.Scan()
		block2 = strings.Split(scanner.Text(), " ")

		var line []int
		var resN int
		for j := 0; j < n; j++ {
			resN, err = strconv.Atoi(block2[j])
			line = append(line, resN)
		}
		A = append(A, line)
	}

	scanner.Scan() //---

	//matB m x p
	for i := 0; i < n; i++ {
		scanner.Scan()
		block2 = strings.Split(scanner.Text(), " ")

		var line []int
		var resN int
		for j := 0; j < p; j++ {
			resN, err = strconv.Atoi(block2[j])
			line = append(line, resN)				
		}
		B = append(B, line)
	}

	file.Close()

	return A, B, m, n, p
}

func check(e error) {
    if e != nil {
        panic(e)
    }
}