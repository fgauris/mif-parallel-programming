#include "OpenMPTests.h"
#include <iostream>
#include <omp.h>
#include "MatricesReadResult.h"
#include "MatrixReader.h"
#include <chrono>


void OpenMPTests::runTests(int testSize)
{
	cout << "OpenMP tests: " << endl;

	/*cout << "1x2x1: ";
	runTest(testSize, ".\\Matrices\\1x2x1.txt");
	cout << "5x4x3: ";
	runTest(testSize, ".\\Matrices\\5x4x3.txt");*/

	cout << "90x120x60: ";
	runTest(testSize, ".\\Matrices\\90x120x60.txt");
	cout << "400x500x600: ";
	runTest(testSize, ".\\Matrices\\400x500x600.txt");
	cout << "900x1100x1000: ";
	runTest(testSize, ".\\Matrices\\900x1100x1000.txt");
	/*
	90x120x60: Average time after 100 runs: 0.1.
	400x500x600: Average time after 100 runs: 77.69.
	900x1100x1000: Average time after 100 runs: 1627.96.
	*/
	cout << endl;
}

void OpenMPTests::runTest(int testSize, string filePath) {
	int c = 0;
	MatrixReader reader = MatrixReader();
	MatricesReadResult matrices = reader.ReadMatrices(filePath);
	int** A = matrices.A;
	//reader.PrintMatrix(A, matrices.m, matrices.n);
	int** B = matrices.B;
	//reader.PrintMatrix(B, matrices.n, matrices.p);

	int totalTime = 0;
	for (int i = 0; i < testSize; i++)
	{
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
		int** C = Multiply(matrices);
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		//std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms" << std::endl;
		totalTime += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
		c = C[0][0];
		//reader.PrintMatrix(C, matrices.m, matrices.p);
	}

	std::cout << "Average time after " << testSize << " runs: " << (totalTime * 1.0 / testSize) << "." << std::endl;
	to_string(c);
}

int** OpenMPTests::Multiply(MatricesReadResult& matrices) {
	int** A = matrices.A;
	int** B = matrices.B;
	int m = matrices.m;
	int n = matrices.n;
	int p = matrices.p;
	int** C = new int* [m];
	for (int l = 0; l < m; l++)
		C[l] = new int[p];

#pragma omp parallel for
	for (int i = 0; i < m; ++i)
	{
#pragma omp parallel for
		for (int j = 0; j < p; ++j)
		{
			C[i][j] = 0;
			for (int k = 0; k < n; ++k) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}

	return C;
}