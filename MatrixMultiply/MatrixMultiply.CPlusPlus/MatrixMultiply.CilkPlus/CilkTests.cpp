#include "CilkTests.h"
#include <iostream>
#include <cilk/cilk.h>
#include "MatricesReadResult.h"
#include "MatrixReader.h"
#include <chrono>
#include <string>

void CilkTests::runTests(int testSize)
{
	cout << "Cilk tests: " << endl;
	cout << "90x120x60: ";
	runTest(testSize, ".\\Matrices\\90x120x60.txt");
	cout << "400x500x600: ";
	runTest(testSize, ".\\Matrices\\400x500x600.txt");
	cout << "900x1100x1000: ";
	runTest(testSize, ".\\Matrices\\900x1100x1000.txt");
	/*
	90x120x60: Average time after 100 runs: 0.1.
	400x500x600: Average time after 100 runs: 77.69.
	900x1100x1000: Average time after 100 runs: 1627.96.
	*/
	cout << endl;
}

void CilkTests::runTest(int testSize, string filePath) {
	int c = 0;
	MatrixReader reader = MatrixReader();
	MatricesReadResult matrices = reader.ReadMatrices(filePath);
	int** A = matrices.A;
	//reader.PrintMatrix(A, matrices.m, matrices.n);
	int** B = matrices.B;
	//reader.PrintMatrix(B, matrices.n, matrices.p);

	int totalTime = 0;
	for (int i = 0; i < testSize; i++)
	{
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
		int** C = Multiply(matrices);
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		//std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms" << std::endl;
		totalTime += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
		c = C[0][0];
	}

	std::cout << "Average time after " << testSize << " runs: " << (totalTime * 1.0 / testSize) << "." << std::endl;
	//reader.PrintMatrix(C, matrices.m, matrices.p);
	to_string(c);
}

int** CilkTests::Multiply(MatricesReadResult& matrices) {
	int** A = matrices.A;
	int** B = matrices.B;
	int m = matrices.m;
	int n = matrices.n;
	int p = matrices.p;
	int** C = new int* [m];
	for (int l = 0; l < m; l++)
		C[l] = new int[p];

	//cilk_for cilk_for for was the most efficient. Keeping it.
	cilk_for(int i = 0; i < m; ++i)
	{
		cilk_for(int j = 0; j < p; ++j)
		{
			C[i][j] = 0;
			for (int k = 0; k < n; ++k) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}

	return C;
}