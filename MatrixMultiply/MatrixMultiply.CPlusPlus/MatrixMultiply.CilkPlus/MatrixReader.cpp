
#include "MatrixReader.h"
#include <iostream>
#include <fstream>
#include <string>
#include <boost/algorithm/string.hpp>
#include <vector>
#include "MatricesReadResult.h"
using namespace std;
using namespace boost;

/* Example: m n p
5 4 3
---
4 3 17 5
4 16 17 15
18 17 10 18
11 12 10 13
18 18 11 14
---
18 9 1
3 2 1
2 18 9
5 4 1
---
140 368 165
229 434 188
485 448 143
319 355 126
470 452 149
*/
MatricesReadResult MatrixReader::ReadMatrices(string filePath) {
	MatricesReadResult result = MatricesReadResult();
	ifstream myReadFile;
	myReadFile.open(filePath);
	string line;
	if (myReadFile.is_open()) {
		vector<string> strs;
		getline(myReadFile, line);
		split(strs, line, is_any_of(" "));
		int m = stoi(strs.at(0));
		int n = stoi(strs.at(1));
		int p = stoi(strs.at(2));
		result.m = m;
		result.n = n;
		result.p = p;
		getline(myReadFile, line);

		/* Setting A matrix: */
		result.A = ReadMatrix(m, n, myReadFile);
		getline(myReadFile, line);
		result.B = ReadMatrix(n, p, myReadFile);
		getline(myReadFile, line);
	}

	myReadFile.close();


	return result;
}

int** MatrixReader::ReadMatrix(int x, int y, ifstream& file)
{
	vector<string> strs;
	string line;
	int** a = new int* [x];
	for (int i = 0; i < x; i++)
		a[i] = new int[y];
	for (int i = 0; i < x; i++)
	{
		getline(file, line);
		vector<string> lineItems = split(strs, line, is_any_of(" "));
		for (int j = 0; j < lineItems.size(); j++)
		{
			a[i][j] = stoi(lineItems.at(j));
		}
	}
	return a;
}



void MatrixReader::PrintMatrix(int** matrix, int x, int y)
{
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			cout << matrix[i][j] << " ";
		}
		cout << "\n";
	}
	cout << "\n";
}

