// MatrixMultiply.Tests.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "SerialTests.h"
#include "CilkTests.h"
#include "TbbTests.h"
#include "OpenMPTests.h"

int main()
{
	int testSize = 100;
	
	/*SerialTests serialTests = SerialTests();
	serialTests.runTests(testSize);*/

	CilkTests cilkTests = CilkTests();
	cilkTests.runTests(testSize);

	/*TbbTests tbbTests = TbbTests();
	tbbTests.runTests(testSize);*/

	OpenMPTests  ompTests = OpenMPTests();
	ompTests.runTests(testSize);
}



