#pragma once
#include <iostream>
#include "MatricesReadResult.h"
using namespace std;

class TbbTests
{
public:
	void runTests(int testSize);
private:
	void runTest(int testSize, string filePath);
	int** Multiply(MatricesReadResult& matrices);


};

