#include "TbbTests.h"
#include <iostream>
#include <tbb/tbb.h>
#include "MatricesReadResult.h"
#include "MatrixReader.h"
#include <chrono>
using namespace tbb;


void TbbTests::runTests(int testSize)
{
	cout << "TBB tests: " << endl;
	cout << "90x120x60: ";
	runTest(testSize, ".\\Matrices\\90x120x60.txt");
	cout << "400x500x600: ";
	runTest(testSize, ".\\Matrices\\400x500x600.txt");
	cout << "900x1100x1000: ";
	runTest(testSize, ".\\Matrices\\900x1100x1000.txt");
	/*
	90x120x60:
	400x500x600:
	900x1100x1000:
	*/
	cout << endl;
}

void TbbTests::runTest(int testSize, string filePath)
{
	int c = 0;
	MatrixReader reader = MatrixReader();
	MatricesReadResult matrices = reader.ReadMatrices(filePath);
	int** A = matrices.A;
	//reader.PrintMatrix(A, matrices.m, matrices.n);
	int** B = matrices.B;
	//reader.PrintMatrix(B, matrices.n, matrices.p);

	int totalTime = 0;
	for (int i = 0; i < testSize; i++)
	{
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
		int** C = Multiply(matrices);
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		totalTime += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
		c = C[0][0];
	}

	std::cout << "Average time after " << testSize << " runs: " << (totalTime * 1.0 / testSize) << "." << std::endl;
	//reader.PrintMatrix(C, matrices.m, matrices.p);
	to_string(c);
}

int** TbbTests::Multiply(MatricesReadResult& matrices) {
	int** A = matrices.A;
	int** B = matrices.B;
	int m = matrices.m;
	int n = matrices.n;
	int p = matrices.p;
	int** C = new int* [m];
	for (int l = 0; l < m; l++)
		C[l] = new int[p];
	tbb::parallel_for(size_t(0), size_t(m), [&](int i)
		{

			tbb::parallel_for(size_t(0), size_t(p), [&](int j)
				//for (int j = 0; j < p; ++j)
				{
					C[i][j] = 0;
					for (int k = 0; k < n; ++k) {
						C[i][j] += A[i][k] * B[k][j];
					}
				});
		});

	return C;
}
