// MatrixMultiply.TBB.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "TbbTests.h"

int main()
{
	int testSize = 100;

	TbbTests tbbTests = TbbTests();
	tbbTests.runTests(testSize);
}

