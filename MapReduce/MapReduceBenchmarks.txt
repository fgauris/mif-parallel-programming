2019-12-24:
Serial tests:
2K: 	Average time after 1 runs: 19 ms.
200K:	Average time after 1 runs: 1953 ms.
500K:	Average time after 1 runs: 5375 ms.
5M:		Average time after 1 runs: 49399 ms.

-----------------------------------------------------------------------------------------------------------------------------------
2019-12-24 17:24:
Serial tests:
12:		Average time after 1 runs: 0 ms.
2K:		Average time after 1 runs: 0 ms.
200K:	Average time after 1 runs: 38 ms.
500K:	Average time after 1 runs: 91 ms.
5M:		Average time after 1 runs: 935 ms.

OpenMP tests:
10:		Average time after 1 runs: 4 ms.
2K:		Average time after 1 runs: 0 ms.
200K:	Average time after 1 runs: 24 ms.
500K:	Average time after 1 runs: 60 ms.
5M:		Average time after 1 runs: 637 ms.

Cilk tests:
10:		Average time after 1 runs: 5 ms.
2K:		Average time after 1 runs: 0 ms.
200K:	Average time after 1 runs: 19 ms.
500K:	Average time after 1 runs: 47 ms.
5M:		Average time after 1 runs: 558 ms.

Tbb tests:
10:		Average time after 1 runs: 22 ms.
2K:		Average time after 1 runs: 0 ms.
200K:	Average time after 1 runs: 24 ms.
500K:	Average time after 1 runs: 49 ms.
5M:		Average time after 1 runs: 548 ms.
----------------------------------------------------------------
2019-12-24 18:35 Chunk size: 1000
Serial tests:
12:		Average time after 20 runs: 0 ms.
2K:		Average time after 20 runs: 0 ms.
200K:	Average time after 20 runs: 44.55 ms.
500K:	Average time after 20 runs: 124.3 ms.
5M:		Average time after 20 runs: 1187.05 ms.

OpenMP tests:
10:		Average time after 20 runs: 0.05 ms.
2K:		Average time after 20 runs: 0 ms.
200K:	Average time after 20 runs: 38 ms.
500K:	Average time after 20 runs: 112.35 ms.
5M:		Average time after 20 runs: 985.4 ms.

Cilk tests:
10:		Average time after 20 runs: 0 ms.
2K:		Average time after 20 runs: 0.1 ms.
200K:	Average time after 20 runs: 30.95 ms.
500K:	Average time after 20 runs: 90.7 ms.
5M:		Average time after 20 runs: 873.3 ms.

Tbb tests:
10:		Average time after 20 runs: 0.05 ms.
2K:		Average time after 20 runs: 0 ms.
200K:	Average time after 20 runs: 30.65 ms.
500K:	Average time after 20 runs: 90.35 ms.
5M:		Average time after 20 runs: 868.1 ms.
---------------------------------------------------------------------
2019-12-24 21:15 Chunk size: 3000
Serial tests:
12:		Average time after 15 runs: 0 ms.
2K:		Average time after 15 runs: 0 ms.
200K:	Average time after 15 runs: 26.4667 ms.
500K:	Average time after 15 runs: 67.8667 ms.
5M:		Average time after 15 runs: 695.533 ms.

OpenMP tests:
10:		Average time after 15 runs: 0.0666667 ms.
2K:		Average time after 15 runs: 0 ms.
200K:	Average time after 15 runs: 16 ms.
500K:	Average time after 15 runs: 38.2 ms.
5M:		Average time after 15 runs: 439.8 ms.

Cilk tests:
10:		Average time after 15 runs: 0.0666667 ms.
2K:		Average time after 15 runs: 0 ms.
200K:	Average time after 15 runs: 14.2667 ms.
500K:	Average time after 15 runs: 33.4667 ms.
5M:		Average time after 15 runs: 367.533 ms.

Tbb tests:
10:		Average time after 15 runs: 0.0666667 ms.
2K:		Average time after 15 runs: 0 ms.
200K:	Average time after 15 runs: 13.3333 ms.
500K:	Average time after 15 runs: 32.6 ms.
5M:		Average time after 15 runs: 365.267 ms.

Go tests:
12:		Average time after 100 runs: 0.01 ms.
2K:		Average time after 100 runs: 0.17 ms.
200K:	Average time after 100 runs: 5.93 ms.
500K:	Average time after 100 runs: 14.79 ms.
5M:		Average time after 100 runs: 129.585 ms.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2019-12-25 18:07
Chunk size: 3000
Serial tests:
12:		Average time after 100 runs: 0 ms.
2K:		Average time after 100 runs: 0 ms.
200K:	Average time after 100 runs: 22.9 ms.
500K:	Average time after 100 runs: 60.1 ms.
5M:		Average time after 100 runs: 615.2 ms.

OpenMP tests:
10:		Average time after 100 runs: 0.2 ms.
2K:		Average time after 100 runs: 0 ms.
200K:	Average time after 100 runs: 10.6 ms.
500K:	Average time after 100 runs: 22.3 ms.
5M:		Average time after 100 runs: 202.5 ms.

Cilk tests:
10:		Average time after 100 runs: 0 ms.
2K:		Average time after 100 runs: 0 ms.
200K:	Average time after 100 runs: 10.7 ms.
500K:	Average time after 100 runs: 20.3 ms.
5M:		Average time after 100 runs: 215.4 ms.

Tbb tests:
10:		Average time after 100 runs: 0.1 ms.
2K:		Average time after 100 runs: 0 ms.
200K:	Average time after 100 runs: 7.1 ms.
500K:	Average time after 100 runs: 17.8 ms.
5M:		Average time after 100 runs: 184.8 ms.

Go tests:
12:		Average time after 100 runs: 0.01 ms.
2K:		Average time after 100 runs: 0.17 ms.
200K:	Average time after 100 runs: 5.93 ms.
500K:	Average time after 100 runs: 14.79 ms.
5M:		Average time after 100 runs: 129.585 ms.