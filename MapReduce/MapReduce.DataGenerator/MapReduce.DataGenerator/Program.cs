﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MapReduce.DataGenerator
{
    class Program
    {


        static void Main(string[] args)
        {
            Console.WriteLine("Generating 10 numbers...");
            GenerateArray(10, "10.txt");

            Console.WriteLine("Generating 2K numbers...");
            GenerateArray(2 * 1000, "2K.txt");

            Console.WriteLine("Generating 200K numbers...");
            GenerateArray(200 * 1000, "200K.txt");

            Console.WriteLine("Generating 500K numbers...");
            GenerateArray(500 * 1000, "500K.txt");

            Console.WriteLine("Generating 5M numbers...");
            GenerateArray(5 * 1000 * 1000, "5M.txt");

            Console.WriteLine("Generating 20M numbers...");
            GenerateArray(20 * 1000 * 1000, "20M.txt");

            //Console.WriteLine("Generating 50M numbers...");
            //GenerateArray(50 * 1000 * 1000, "50M.txt");

            //Console.WriteLine("Generating 100M numbers...");
            //GenerateArray(100 * 1000 * 1000, "100M.txt");

            //Console.WriteLine("Generating 200M numbers...");
            //GenerateArray(200 * 1000 * 1000, "200M.txt");

            Console.WriteLine("Generated. Press any key to continue..");
            Console.Read();
        }

        private static void GenerateArray(int size, string fileName)
        {
            var rand = new Random();
            const int split = 100 * 1000;
            using var file = new StreamWriter(fileName);
            file.WriteLine(size);
            for (var i = 0; i < size-1; i++)
            {
                file.Write(rand.Next(byte.MinValue, byte.MaxValue) + " ");
                if (i > 0 && i % split == 0)
                {
                    file.WriteLine();
                }
            }
            file.Write(rand.Next(short.MaxValue));
        }
    }
}
