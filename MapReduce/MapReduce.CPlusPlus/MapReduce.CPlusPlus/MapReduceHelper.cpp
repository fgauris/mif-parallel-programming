#include "MapReduceHelper.h"


std::map<int, int> MapReduceHelper::map(DataReadResult* chunk)
{
	std::map<int, int> map;
	int length = chunk->length;
	for (int i = 0; i < length; i++)
	{
		int elem = chunk->numbers[i];
		if (map.count(elem) > 0)
			map[elem] = map[elem] + 1;
		else
			map.insert({ elem, 1 });
	}

	return map;
}

std::map<int, int> MapReduceHelper::reduce(std::vector<std::map<int, int>>& vector)
{
	std::map<int, int> result;
	for(auto const& chunk : vector)
	{
		for (auto const& x : chunk)
		{
			int key = x.first;
			int value = x.second;

			if (result.count(key) > 0)
				result[key] = result[key] + value;
			else
				result.insert({ key, value });
		}
	}


	return result;
}

