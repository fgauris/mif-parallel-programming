#pragma once
#include <iostream>
#include "DataReadResult.h"
#include "TestData.h"
#include <map>
#include <vector>
using namespace std;

class SerialTests
{
public:
	void runTests(int testSize, TestData* testData);
private:
	void runTest(int testSize, DataReadResult* data, bool printData);
	map<int, int> mapReduceSerial(vector<DataReadResult*>& chunks);
};

