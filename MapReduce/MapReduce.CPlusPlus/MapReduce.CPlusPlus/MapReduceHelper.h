#include <map>
#include <vector>
#include "DataReadResult.h"
#include "MapResult.h"

#pragma once
class MapReduceHelper
{
public:
	static std::map<int, int> map(DataReadResult* chunk);
	static std::map<int, int> reduce(std::vector<std::map<int, int>>& chunk);
	
};

