#include "SerialTests.h"
#include "DataReadResult.h"
#include "DataReader.h"
#include "MapReduceHelper.h"
#include "MapResult.h"
#include <iostream>
#include <boost/format.hpp>
#include <cmath>
#include <chrono>
#include <thread>
#include <map>

void SerialTests::runTests(int testSize, TestData* testData) 
{
	cout << boost::format("Serial tests:") << endl;
	cout << "12:" << endl;
	runTest(testSize, testData->Size12, false);
	cout << "2K:" << endl;
	runTest(testSize, testData->Size2K, false);
	cout << "200K:" << endl;
	runTest(testSize, testData->Size200K, false);
	cout << "500K:" << endl;
	runTest(testSize, testData->Size500K, false);
	cout << "5M:" << endl;
	runTest(testSize, testData->Size5M, false);
	/*cout << "20M:" << endl;
	runTest(testSize, testData->Size20M, false);*/
	/*cout << "100M:" << endl;
	runTest(testSize, "..\\..\\Data\\100M.txt", false);*/
	cout << endl;
}

void SerialTests::runTest(int testSize, DataReadResult* data, bool printData) {
	DataReader reader = DataReader();
	int* numbers = data->numbers;
	if (printData)
	{
		cout << "Original numbers: ";
		reader.printNumbers(*data);
	}

	int totalTime = 0;
	for (int i = 0; i < testSize; i++)
	{
		std::vector<DataReadResult*> chunks = reader.getChunks(*data);
		if (i == 0 && printData) 
		{
			cout << "Chunks:" << endl;
			reader.printChunks(chunks);
		}
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
		std::map<int, int> results = mapReduceSerial(chunks);
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		totalTime += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
		if (i == 0 && (printData || DataReader::printFinalResults))
		{
			cout << "Results: ";
			reader.printMap(results);
		}
	}

	std::cout << "Average time after " << testSize << " runs: " << (totalTime * 1.0 / testSize) << " ms." << std::endl;
}

std::map<int, int> SerialTests::mapReduceSerial(std::vector<DataReadResult*>& chunks) {
	int chunkSize = chunks.size();
	std::vector<map<int, int>> mapResults;
	mapResults.resize(chunkSize);
	for (int i = 0; i < chunkSize; i++)
	{
		map<int, int> mapResult = MapReduceHelper::map(chunks[i]);
		mapResults.push_back(mapResult);
	}

	map<int, int> result = MapReduceHelper::reduce(mapResults);
	return result;

	//return {};
}




