#pragma once
#include <iostream>
#include "DataReadResult.h"
#include "TestData.h"
#include "tbb/task_group.h"
#include <map>
#include <vector>
using namespace tbb;
using namespace std;

class TbbTests
{
public:
	void runTests(int testSize, TestData* testData);
private:
	void runTest(int testSize, DataReadResult* data, bool printData);
	map<int, int> mapReduceParallel(std::vector<DataReadResult*>& chunks);
	/*void quickSort(DataReadResult& data, int low, int high, task_group& g);*/
};


