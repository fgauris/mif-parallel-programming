#pragma once
#include <iostream>
#include "DataReadResult.h"
#include "TestData.h"
#include <map>
#include <vector>
using namespace std;

class CilkTests
{
public:
	void runTests(int testSize, TestData* testData);
private:
	void runTest(int testSize, DataReadResult* data, bool printData);
	map<int, int> mapReduceParallel(std::vector<DataReadResult*>& chunks);
};

