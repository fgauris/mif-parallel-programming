#pragma once
#include <string>
#include "DataReadResult.h"
#include "TestData.h"
#include <vector>
#include <map>

class DataReader
{
public:
	TestData* readAllData();
	DataReadResult* readData(std::string filePath);
	void printNumbers(DataReadResult& data);
	DataReadResult copyArray(DataReadResult& data);
	std::vector<DataReadResult*> getChunks(DataReadResult& data);
	void printChunks(std::vector<DataReadResult*>& chunks);
	void printMap(std::map<int, int>& map);
	static const int chunkSize = 3000;
	static const bool printFinalResults = false;
};

