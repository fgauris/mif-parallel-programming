#include "DataReader.h"
#include <iostream>
#include <fstream>
#include <string>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <map>
#include <sstream>
#include "DataReadResult.h"
#include <stdexcept>
using namespace std;
using namespace boost;


TestData* DataReader::readAllData()
{
	cout << "Reading 12..." << endl;
	DataReadResult* size12 = readData("..\\..\\Data\\12.txt");
	cout << "Reading 2K..." << endl;
	DataReadResult* size2K = readData("..\\..\\Data\\2K.txt");
	cout << "Reading 200K..." << endl;
	DataReadResult* size200K = readData("..\\..\\Data\\200K.txt");
	cout << "Reading 500K..." << endl;
	DataReadResult* size500K = readData("..\\..\\Data\\500K.txt");
	cout << "Reading 5M..." << endl;
	DataReadResult* size5M = readData("..\\..\\Data\\5M.txt");
	/*cout << "Reading 20M..." << endl;
	DataReadResult* size20M = readData("..\\..\\Data\\20M.txt");*/

	TestData* result = new TestData(size12, size2K, size200K, size500K, size5M);

	return result;
}

DataReadResult* DataReader::readData(std::string filePath) {
	ifstream myReadFile;
	myReadFile.open(filePath);
	string line;
	int* arr;
	int arrLength = 0;
	int lineLength = 0;
	int start = 0;
	int end = 0;
	if (myReadFile.is_open()) {
		getline(myReadFile, line);
		arrLength = stoi(line);
		arr = new int[arrLength];

		while (getline(myReadFile, line)) {
			vector<string> strs;
			vector<string> lineItems = split(strs, line, is_any_of(" "));
			if (lineItems[lineItems.size() - 1].empty())
				lineItems.pop_back();
			lineLength = lineItems.size();
			end += lineLength;
			for (int i = 0; i < lineLength; i++)
			{
				arr[start + i] = stoi(lineItems[i]);
			}
			start = end;
		}
	}
	else
	{
		throw std::invalid_argument("File not found! File: " + filePath);
	}
	return new DataReadResult(arr, arrLength);
}

void DataReader::printNumbers(DataReadResult& data) {
	for (int i = 0; i < data.length; ++i)
	{
		std::cout << (int)data.numbers[i] << " ";
	}
	cout << endl;
}

DataReadResult DataReader::copyArray(DataReadResult& data) {
	int* dest = new int[data.length];
	std::copy(data.numbers, data.numbers + data.length, dest);
	return DataReadResult(dest, data.length);
}

vector<DataReadResult*> DataReader::getChunks(DataReadResult& data) {
	vector<DataReadResult*> result;
	int chunkSize = DataReader::chunkSize;
	int chunks = data.length / chunkSize;

	for (int i = 0; i < chunks; i++)
	{
		int from = chunkSize * i;
		int to = from + chunkSize;
		int length = to - from;
		int* dest = new int[length];
		std::copy(data.numbers + from, data.numbers + to, dest);

		DataReadResult* chunk = new DataReadResult(dest, length);
		result.push_back(chunk);
	}
	int left = data.length - chunks * chunkSize;
	if (left  > 0)
	{
		int from = chunks * chunkSize;
		int to = from + left;
		int* dest = new int[left];
		std::copy(data.numbers + from, data.numbers + to, dest);

		DataReadResult* chunk = new DataReadResult(dest, left);
		result.push_back(chunk);
	}

	return result;
}

void DataReader::printChunks(vector<DataReadResult*>& chunks) {
	int size = chunks.size();
	for (int i = 0; i < size; i++)
	{
		cout << "Chunk " << i << ": ";
		DataReadResult* chunk = chunks[i];
		for (int j = 0; j < chunk->length; j++)
		{
			cout << chunk->numbers[j] << " ";
		}
		cout << endl;
	}
}

void DataReader::printMap(std::map<int, int>& map) {
	for (auto const& x : map)
	{
		std::cout << x.first << ':' << x.second << ' ';
	}
	cout << endl;
}
