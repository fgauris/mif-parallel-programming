#include "DataReadResult.h";
#pragma once
class TestData
{
public:
	TestData(DataReadResult* size12, DataReadResult* size2K, 
		DataReadResult* size200K, DataReadResult* size500K,
		DataReadResult* size5M/*, DataReadResult* size20M*/) {
		this->Size12 = size12;
		this->Size2K = size2K;
		this->Size200K = size200K;
		this->Size500K = size500K;
		this->Size5M = size5M;
		//this->Size20M = size20M;
	}
	~TestData() {
		delete this->Size12;
		delete this->Size2K;
		delete this->Size200K;
		delete this->Size500K;
		delete this->Size5M;
		//delete this->Size20M;
	}

	DataReadResult* Size12;
	DataReadResult* Size2K;
	DataReadResult* Size200K;
	DataReadResult* Size500K;
	DataReadResult* Size5M;
	//DataReadResult* Size20M;
};

