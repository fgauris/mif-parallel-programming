#include "CilkTests.h"
#include "DataReadResult.h"
#include "DataReader.h"
#include "MapReduceHelper.h"
#include <iostream>
#include <boost/format.hpp>
#include <cilk/cilk.h>
#include <cilk/reducer_list.h>
#include <list>
#include <cmath>
#include <chrono>
#include <thread>

void CilkTests::runTests(int testSize, TestData* testData)
{
	cout << boost::format("Cilk tests:") << endl;
	cout << "10:" << endl;
	runTest(testSize, testData->Size12, false);
	cout << "2K:" << endl;
	runTest(testSize, testData->Size2K, false);
	cout << "200K:" << endl;
	runTest(testSize, testData->Size200K, false);
	cout << "500K:" << endl;
	runTest(testSize, testData->Size500K, false);
	cout << "5M:" << endl;
	runTest(testSize, testData->Size5M, false);
	/*cout << "20M:" << endl;
	runTest(testSize, testData->Size20M, false);*/
	/*cout << "100M:" << endl;
	runTest(testSize, "..\\..\\Data\\100M.txt", false);*/
	cout << endl;
}


void CilkTests::runTest(int testSize, DataReadResult* data, bool printData) {
	DataReader reader = DataReader();
	int* numbers = data->numbers;
	if (printData)
	{
		cout << "Original numbers: ";
		reader.printNumbers(*data);
	}

	int totalTime = 0;
	for (int i = 0; i < testSize; i++)
	{
		std::vector<DataReadResult*> chunks = reader.getChunks(*data);
		if (i == 0 && printData)
		{
			cout << "Chunks:" << endl;
			reader.printChunks(chunks);
		}
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
		std::map<int, int> results = mapReduceParallel(chunks);
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		totalTime += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
		if (i == 0 && (printData || DataReader::printFinalResults))
		{
			cout << "Results: ";
			reader.printMap(results);
		}
	}

	std::cout << "Average time after " << testSize << " runs: " << (totalTime * 1.0 / testSize) << " ms." << std::endl;
}

std::map<int, int> CilkTests::mapReduceParallel(std::vector<DataReadResult*>& chunks) {
	//
	cilk::reducer< cilk::op_list_append<map<int, int>> > mapResults;
	
	int chunkSize = chunks.size();
	cilk_for (int i = 0; i < chunkSize; i++)
	{
		map<int, int> mapResult = MapReduceHelper::map(chunks[i]);
		mapResults->push_back(mapResult);
	}
	const std::list<map<int, int>>& list = mapResults.get_value();

	std::vector<map<int, int>> vector;
	vector.resize(chunkSize);
	for (auto const& m : list)
	{
		vector.push_back(m);
	}
	map<int, int> result = MapReduceHelper::reduce(vector);
	return result;

	//return {};
}
