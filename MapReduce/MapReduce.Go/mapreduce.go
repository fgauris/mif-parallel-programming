package main

import (
	"bufio"
	"fmt"
	"io"
	"sort"
	"strconv"
	"sync"
	"time"

	// "fmt"
	"os"
	"strings"
	//"log"
	//"sync"

	"container/list"
)

var ChunkSize int = 3000
var PrintFinalResults bool = false

func main() {
	testSize := 100
	runTests(testSize)
}

func runTests(testSize int) {
	fmt.Println("Google Go tests:")
	fmt.Println("12:")
	runTest(testSize, "..\\Data\\12.txt", false)
	fmt.Println("2K:")
	runTest(testSize, "..\\Data\\2K.txt", false)
	fmt.Println("200K:")
	runTest(testSize, "..\\Data\\200K.txt", false)
	fmt.Println("500K:")
	runTest(testSize, "..\\Data\\500K.txt", false)
	fmt.Println("5M:")
	runTest(testSize, "..\\Data\\5M.txt", false)


	//fmt.Println("20M:")
	//runTest(testSize, "..\\Data\\20M.txt", false)

}

func runTest(testSize int, filePath string, printResults bool) {
	numbers, _ := readData(filePath)
	if printResults {
		fmt.Print("Original numbers: ")
		printNumbers(numbers)
	}

	var totalTime int64 = 0
	for i := 0; i < testSize; i++ {
		chunks := getChunks(numbers)
		if i == 0 && printResults {
			fmt.Println("Chunks:")
			printChunks(chunks)
		}
		begin := time.Now()
		results := mapReduceParallel(chunks)
		durationNano := time.Since(begin).Nanoseconds()
		totalTime += (int64)(durationNano / 10000)
		if i == 0 && (printResults || PrintFinalResults) {
			fmt.Print("Results: ")
			printMap(results)
		}
	}
	fmt.Println("Average time after", testSize, "runs:", float64(totalTime)/float64(testSize)/100, ".")
}

func mapReduceParallel(chunks *list.List) map[int]int{
	//create list mapResults
	chunkSize := chunks.Len()
	var wg sync.WaitGroup
	wg.Add(chunkSize)
	channel := make(chan map[int]int, chunkSize)
	for chunk := chunks.Front(); chunk != nil; chunk = chunk.Next() {
		go func(chunk []int) {
			defer wg.Done()
			mapResult := doMap(chunk)
			channel <- mapResult
		}(chunk.Value.([]int))
	}
	wg.Wait()
	close(channel)

	mapResults := list.New()
	for el := range channel {
		mapResults.PushBack(el)
	}

	result := doReduce(mapResults)
	return result
}

func doMap(chunk []int) map[int]int{
	result := make(map[int]int)

	for _, elem := range chunk {
		_, exists := result[elem]
		if exists {
			result[elem] = result[elem] + 1
		} else {
			result[elem] = 1
		}
	}

	return result
}

func doReduce(chunks *list.List) map[int]int{
	result := make(map[int]int)
	for chunk := chunks.Front(); chunk != nil; chunk = chunk.Next() {
		mp := chunk.Value.(map[int]int)
		for key, value := range mp {
			_, exists := result[key]
			if exists {
				result[key] = result[key] + value
			} else {
				result[key] = value
			}
		}
	}

	return result
}

func readData(filePath string) ([]int, int) {
	file, err := os.Open(filePath)
	check(err)
	defer file.Close()
	reader := bufio.NewReader(file)
	var arrayLengthS string
	arrayLengthS, err = reader.ReadString('\n')
	arrayLengthS = strings.ReplaceAll(arrayLengthS, "\r\n", "")
	check(err)
	var arrayLength int
	arrayLength, err = strconv.Atoi(arrayLengthS)
	check(err)
	arr := make([]int, arrayLength)
	var line string
	i := 0
	for {

		line, err = reader.ReadString('\n')
		line = strings.ReplaceAll(line, "\r\n", "")

		strs := strings.Split(line, " ")
		var err1 error
		for j := 0; j < len(strs);j++  {
			if strs[j] != "" {
				arr[i], err1 = strconv.Atoi(strs[j])
				check(err1)
				i++
			}
		}

		if err != nil {
			break
		}
	}

	//fmt.Printf("\tRead %d numbers\n", len(arr))

	//fmt.Printf(" \tLast element: %v \n", arr[len(arr)-1])

	if err != io.EOF {
		fmt.Printf("Failed!: %v\n", err)
	}

	return arr, arrayLength
}

func printNumbers(numbers []int) {
	length := len(numbers)
	for i := 0; i < length; i++ {
		fmt.Printf("%v ", numbers[i])
	}
	fmt.Println()
}

func printChunks(chunks *list.List) {
	i := 0
	for chunk := chunks.Front(); chunk != nil; chunk = chunk.Next() {
		fmt.Printf("Chunk %v: ", i)
		i++
		chunkAsArray := chunk.Value.([]int)
		for j := 0; j < len(chunkAsArray); j++  {
			fmt.Printf("%v ", chunkAsArray[j])
		}
		fmt.Println()
	}
		
}

func printMap(m map[int]int){
	var keys []int
	for k := range m {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	for _, k := range keys {
		fmt.Printf("%v:%v ", k, m[k])
	}
	fmt.Println()
}

func getChunks (data []int) *list.List {
	result := list.New()
	chunkSize := ChunkSize
	chunks := len(data) / chunkSize

	for i := 0; i < chunks; i++ {
		from := chunkSize * i
		to := from + chunkSize
		length := to - from
		dest := make([]int, length)
		for j := 0; j < length; j++ {
			dest[j] = data[from + j]
		}
		result.PushBack(dest)
	}

	left := len(data) - chunks * chunkSize
	if left > 0 {
		from := chunks * chunkSize
		dest := make([]int, left)
		for j := 0; j < left; j++ {
			dest[j] = data[from + j]
		}
		result.PushBack(dest)
	}

	return result;
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
